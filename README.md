# Sistema de Televigilancia Chile 2020

Proyecto de geolocalización de las cámaras de vigilancia que se instalarán en Chile por el Sistema de Teleprotección Nacional que busca articular un sistema integrado y permanente de la vigilancia en Chile. La licitación para la instalación de las cámaras fue adjudicada por la empresa [IngeSmart SA](https://www.ingesmart.cl/). Las cámaras del proyecto están programas para instalarse en las ubicación recogidas en este mapa, por lo que aún no se encuentran operativas, pero podemos saber dónde se instalarán.

## Mapa de cámaras del Sistema de Teleprotección

El mapa puede ser consultado aquí:

https://cdisonancia.gitlab.io/sistema-televigilancia-chile-2020/

Las cuatro marcas son las siguientes:

* **Cámaras PTZ**: **(morado)** poseen un lente capaz de girar en 360° 

  ![ptz](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTW8vCiP1wlT0RnJOvvgg04Rt2k7BBXDOdV2SZ_X8m5NvTLrBQsBA&s)

  

* **Cámaras Panovu**: **(azul)** cuentan con una serie de lentes ordenados en una circunferencia para grabar en 360° simultáneamente, obteniendo imágenes panorámicas.

![panovu](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuu0kJWOBIySFVx0jJ3Obph9L9sRI2wBkchAt6U64vD3XpM8fE&s)

* **Cámaras "*Reconocimiento Facial*"**: **(rojo)** la descripción del documento original del Sistema de Protección identifica con este nombre a las cámaras fijas y unidireccionales que además cuentan con reconocimiento facial. Esto no quiere decir que los modelos anteriores no permitan el reconocimiento facial.

  [cámara_fija]()

* **Centrales de control y monitoreo**: **(verde)** lugares donde se registrará y analizará la vigilancia de las cámaras. Por lo general, son instalaciones municipales o policiales.



## Script y base de datos

En la carpeta "**Script R y Base de Datos**" se encuentra el script en R con el que se elaboró el mapa, principalmente usando la librería Leaftlet. El listado de ubicaciones de las cámaras y de las centrales de monitoreo se extrajeron de los documentos originales de la licitación del proyecto de vigilancia. Se encuentran en formato .csv en la misma carpeta.



## Documentos originales

Los documentos originales del Sistema de Teleprotección Nacional son públicos. Pueden descargarse aún en los sitios originales de su publicación:

* [Licitación de cámaras](http://www.mercadopublico.cl/Procurement/Modules/RFB/DetailsAcquisition.aspx?qs=ONMvVsNW0cO2femUYS6J+g==)
* [Copia web de licitación en archive.org](https://web.archive.org/web/20200325184634/http://www.mercadopublico.cl/Procurement/Modules/RFB/DetailsAcquisition.aspx?qs=ONMvVsNW0cO2femUYS6J+g%3D%3D)
* [Descripción Sistema de Teleprotección](http://www.seguridadpublica.gov.cl/media/2019/07/Sistemas-de-Teleproteccion.pdf)

Si no están disponibles, se pueden descargar en [esta recopilación que hicimos](https://cloud.disroot.org/s/ZePqKpESzyNAcTf) o en este [magnet para descargar por torrent](magnet:?xt=urn:btih:a0a39ae4fe4f5ce79a286ad4d50423b4548a493d&dn=Documentos%20Sistema%20de%20Teleprotecci%C3%B3n%202020&tr=udp%3A%2F%2Fpublic.popcorn-tracker.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Fzer0day.ch%3A1337&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce)

.